<?php declare(strict_types=1);

use Drupal\Core\Render\BubbleableMetadata;

/**
 * @file
 * Token integration for the Ecwid module.
 */

function ecwid_token_info() {
  $info = [];

  $info['tokens']['site']['ecwid_store_base_path'] = [
    'name' => t('Ecwid store base path'),
    'description' => t('Prepended to every store URL.')
  ];

  return $info;
}

/**
 * Implements hook_tokens().
 */
function ecwid_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {
  $replacements = [];

  if ($type == 'site') {
    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'ecwid_store_base_path':
          $store_base_path = \Drupal::config('ecwid.settings')->get('store_base_path');
          $replacements[$original] = $store_base_path;
          break;
      }
    }
  }

  return $replacements;
}
