<?php declare(strict_types=1);

namespace Drupal\woolwich_ecwid\Exception;

/**
 * Represents a request to an invalid or malformed Ecwid API path.
 */
class InvalidPathException extends EcwidApiError
{
}
