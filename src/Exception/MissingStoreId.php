<?php declare(strict_types=1);

namespace Drupal\woolwich_ecwid\Exception;

class MissingStoreId extends EcwidApiError
{
}
