<?php declare(strict_types=1);

namespace Drupal\woolwich_ecwid\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\File\FileSystem;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Menu\MenuLinkManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Link;
use Drupal\woolwich_ecwid\EcwidApiService;
use Drupal\path_alias\AliasManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\system\Entity\Menu;

class EcwidPageForm extends ContentEntityForm
{
  /**
   * Allow retrieving / validating information from Ecwid.
   */
  protected EcwidApiService $ecwidApiService;

  protected AliasManagerInterface $aliasManager;

  /**
   * {@inheritDoc}
   */
  public function __construct(
    EntityRepositoryInterface $entity_repository,
    EntityTypeBundleInfoInterface $entity_type_bundle_info,
    TimeInterface $time,
    EcwidApiService $ecwidApiService,
    MessengerInterface $messenger,
    AliasManagerInterface $aliasManager
  ) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);
    $this->ecwidApiService = $ecwidApiService;
    $this->setMessenger($messenger);
    $this->aliasManager = $aliasManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container)
  {
    return new static(
      $container->get("entity.repository"),
      $container->get("entity_type.bundle.info"),
      $container->get("datetime.time"),
      $container->get("ecwid.ecwid_api_service"),
      $container->get("messenger"),
      $container->get("path_alias.manager")
    );
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state)
  {
    // Set the status to published (true) before saving.
    $this->entity->set("status", true);
    parent::save($form, $form_state);

    // Check if the 'Provide a menu link' toggle is enabled.
    if ($form_state->getValue("menu_link_toggle")) {
      // The first item in the string on menu items, is the root menu name.
      $parent = explode(":", $form_state->getValue("parent_menu_item"));
      // Prepare the menu link data.
      $menu_link_data = [
        "title" => $this->entity->label(),
        "menu_name" => $parent[0],
        "parent" => implode(":", array_slice($parent, 1)),
        "weight" => $form_state->getValue("weight"),
        "link" => ["uri" => "entity:ecwid_page/" . $this->entity->id()],
      ];

      // Create or update the menu link.
      $this->updateOrCreateMenuLink($menu_link_data);
    }
  }

  /**
   * Helper function to create or update a menu link.
   */
  private function updateOrCreateMenuLink(array $menu_link_data)
  {
    // Load the menu link if it exists or create a new one.
    /** @var MenuLinkManagerInterface $menuLinkManager */
    $menuLinkManager = \Drupal::service("plugin.manager.menu.link");
    $url = $this->entity->toUrl();
    $existing_menu_link = $menuLinkManager->loadLinksByRoute(
      $url->getRouteName(),
      $url->getRouteParameters()
    );

    if (!empty($existing_menu_link)) {
      // Update existing menu link.
      $menu_link = reset($existing_menu_link);
      // $menu_link->updateLink($menu_link_data, true);
      $menuLinkManager->updateDefinition(
        $menu_link->getPluginId(),
        $menu_link_data,
        true
      );
    } else {
      // Create new menu link.
      \Drupal::entityTypeManager()
        ->getStorage("menu_link_content")
        ->create($menu_link_data)
        ->save();
    }
  }

  /**
   * Builds the entity form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {
    $menuLinkManager = \Drupal::service("plugin.manager.menu.link");
    $menu_links = $menuLinkManager->loadLinksByRoute("entity:ecwid_page/*");

    // Delete each menu link associated with the entity.
    foreach ($menu_links as $menu_link) {
      $menu_link->delete();
    }

    $form["#attached"]["library"][] = "ecwid/ecwid_page_form";
    $form = parent::buildForm($form, $form_state);

    if (!$this->ecwidApiService->validateEcwidStore()) {
      $link = Link::createFromRoute(
        $this->t("Ecwid settings"),
        "ecwid.settings_form"
      )->toString();
      $this->messenger->addWarning(
        $this->t(
          "The Ecwid store is invalid. Please set or update your store id: @link.",
          [
            "@link" => $link,
          ]
        )
      );
      $categories = [];
      $form["actions"]["#disabled"] = true;
    } else {
      $categories = $this->ecwidApiService->getCategories();
    }

    $options = [
      "" => $this->t(" - Select a category - "),
    ];
    foreach ($categories as $category) {
      $options[$category["id"]] = $category["name"];
    }

    // Check if ecwid_category has a selected value
    $ecwidCategoryValue =
      $form_state->getValue("ecwid_category") ??
      $this->entity->ecwid_category->value;

    $form["ecwid_category"] = [
      "#type" => "select",
      "#title" => $this->t("Ecwid Category"),
      "#options" => $options,
      "#default_value" => $ecwidCategoryValue ?? "",
      "#required" => true,
      "#ajax" => [
        "callback" => "::updateFormFields",
        "wrapper" => "category-dependent-fields",
        "event" => "change",
      ],
    ];

    // Container for fields that depend on ecwid_category
    $form["category_dependent_fields"] = [
      "#type" => "container",
      "#attributes" => ["id" => "category-dependent-fields"],
    ];

    $pathField = $form["path"];
    unset($form["path"]);
    $titleField = $form["title"];
    unset($form["title"]);

    if ($ecwidCategoryValue && is_numeric($ecwidCategoryValue)) {
      $category = $this->ecwidApiService->getCategory(
        (int) $ecwidCategoryValue
      );

      // Title field.
      $form["category_dependent_fields"]["title"] = $titleField;

      // Url alias field.
      $pathField["widget"][0]["alias"]["#states"] += [
        "invisible" => [
          'input[name="path[0][path_auto]"]' => ["checked" => true],
        ],
      ];
      $form["category_dependent_fields"]["path"] = $pathField;

      // Menu fields.
      /** @var \Drupal\Core\Menu\MenuParentFormSelectorInterface $menuParentSelector */
      $menuParentSelector = \Drupal::service("menu.parent_form_selector");

      $default = "menu:";
      $pluginId = "";
      if ($this->entity->id()) {
        /** @var \Drupal\Core\Menu\MenuLinkManagerInterface $menuLinkManager */
        $menuLinkManager = \Drupal::service("plugin.manager.menu.link");
        $url = $this->entity->toUrl();
        $existingMenuLinks = $menuLinkManager->loadLinksByRoute(
          $url->getRouteName(),
          $url->getRouteParameters()
        );
        $existingMenuLink = reset($existingMenuLinks);
        if (!empty($existingMenuLink)) {
          $default =
            $existingMenuLink->getMenuName() .
            ":" .
            $existingMenuLink->getParent();
          $pluginId = $existingMenuLink->getPluginId();
        }
      }

      $parentElement = $menuParentSelector->parentSelectElement(
        $default,
        $pluginId,
        [
          "main" => $this->t("Main menu"),
        ]
      );

      // Add 'Provide a menu link' toggle.
      $form["category_dependent_fields"]["menu_link_toggle"] = [
        "#type" => "checkbox",
        "#title" => $this->t("Provide a menu link"),
        "#default_value" => 1,
      ];

      // Add form element for parent menu item select.
      $form["category_dependent_fields"]["parent_menu_item"] = $parentElement;
      $form["category_dependent_fields"]["parent_menu_item"]["#states"][
        "visible"
      ][':input[name="menu_link_toggle"]'] = ["checked" => true];

      // Add field for menu item weight.
      $form["category_dependent_fields"]["weight"] = [
        "#type" => "number",
        "#title" => $this->t("Weight"),
        "#default_value" => 0,
        "#states" => [
          "visible" => [
            ':input[name="menu_link_toggle"]' => ["checked" => true],
          ],
        ],
      ];
    }

    return $form;
  }

  public function updateFormFields(array &$form, FormStateInterface $form_state)
  {
    return $form["category_dependent_fields"];
  }
}
