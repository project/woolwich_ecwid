<?php declare(strict_types=1);

namespace Drupal\woolwich_ecwid\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * @ContentEntityType(
 *   id = "ecwid_page",
 *   label = @Translation("Ecwid Page"),
 *   base_table = "ecwid_page",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "title",
 *     "uuid" = "uuid",
 *     "langcode" = "langcode",
 *     "status" = "status"
 *   },
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "default" = "Drupal\woolwich_ecwid\Form\EcwidPageForm",
 *       "add" = "Drupal\woolwich_ecwid\Form\EcwidPageForm",
 *       "edit" = "Drupal\woolwich_ecwid\Form\EcwidPageForm",
 *       "delete" = "Drupal\woolwich_ecwid\Form\EcwidPageDeleteForm"
 *     },
 *     "access" = "Drupal\Core\Entity\EntityAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *   },
 *   links = {
 *     "canonical" = "/ecwid-page/{ecwid_page}",
 *     "add-form" = "/admin/content/ecwid-page/add",
 *     "edit-form" = "/admin/content/ecwid-page/{ecwid_page}/edit",
 *     "delete-form" = "/admin/content/ecwid-page/{ecwid_page}/delete",
 *     "collection" = "/admin/content/ecwid-pages"
 *   },
 *   admin_permission = "administer ecwid_page entities",
 * )
 */
class EcwidPage extends ContentEntityBase
{
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type)
  {
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields["title"] = BaseFieldDefinition::create("string")
      ->setLabel(t("Title"))
      ->setDescription(t("The title of the Ecwid page."))
      ->setRequired(true)
      ->setSettings(["default_value" => "", "max_length" => 255])
      ->setDisplayOptions("form", [
        "type" => "string_textfield",
      ])
      ->setDisplayConfigurable("form", true)
      ->setDisplayConfigurable("view", true);

    $fields["ecwid_category"] = BaseFieldDefinition::create("list_integer")
      ->setLabel(t("Ecwid Category"))
      ->setDescription(t("The Ecwid category associated with this page."))
      ->setDefaultValue("")
      ->setDisplayConfigurable("form", true)
      ->setDisplayConfigurable("view", true);

    $fields["path"] = BaseFieldDefinition::create("path")
      ->setLabel(t("URL alias"))
      ->setTranslatable(true)
      ->setDisplayOptions("form", [
        "type" => "path",
        "weight" => 30,
      ])
      ->setDisplayConfigurable("form", true)
      ->setComputed(true);

    $fields["status"] = BaseFieldDefinition::create("boolean")
      ->setLabel(t("Published status"))
      ->setDescription(
        t("A boolean indicating whether the Ecwid page is published.")
      )
      ->setSettings(["default_value" => 1])
      ->setDisplayConfigurable("form", true);

    return $fields;
  }
}
