<?php declare(strict_types=1);

namespace Drupal\woolwich_ecwid\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Link;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\woolwich_ecwid\EcwidApiService;
use GuzzleHttp\Exception\ClientException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Ecwid Store block.
 *
 * @Block(
 *   id = "ecwid_store",
 *   admin_label = @Translation("Ecwid Store Block"),
 *   category = @Translation("Ecwid integration")
 * )
 */
class StoreBlock extends BlockBase implements ContainerFactoryPluginInterface {
  public function build(): array {
    $config = $this->getConfiguration();
    $storeConfig = $this->configFactory->get('ecwid.settings');
    $store_id = $storeConfig->get('store_id') ?? '';
    $category_id = $config['ecwid_category'] ?? '';
    $base_path = $storeConfig->get('store_base_path');

    if ($base_path === false) {
      $link = Link::createFromRoute(
        $this->t('Ecwid settings'),
        'ecwid.settings_form',
      )->toString();

      $this->loggerChannelFactory->get('ecwid')->error(
        $this->t(
          'The Ecwid store base path is invalid. Please set or update your store base path (under Advanced): @link.',
          [
            '@link' => $link,
          ],
        ),
      );
    }

    try {
      $category = is_numeric($category_id)
        ? $this->ecwidApiService->getCategory((int) $category_id)
        : null;
    } catch (ClientException $e) {
      $printedError = print_r($e, true);
      $this->loggerChannelFactory
        ->get('ecwid')
        ->error(
          'There was an error fetching the category for a store block. ' .
            'Displaying the store without a default category selected. ' .
            'Maybe the selected category has been deleted from the Ecwid ' .
            "store? The category id is '$category_id' and the error was: $printedError",
        );
    }

    return [
      '#theme' => 'ecwid_store_block',
      '#store_id' => $store_id,
      '#base_path' => $base_path,
      '#store_base_path' => $base_path,
      ...!is_null($category) ? ['#default_category' => $category] : [],
    ];
  }

  public function blockForm($form, FormStateInterface $form_state): array {
    $form = parent::blockForm($form, $form_state);
    $categories = $this->ecwidApiService->getCategories();
    $options = [];

    foreach ($categories as $category) {
      $options[$category['id']] = $category['name'];
    }

    $form['ecwid_category'] = [
      '#type' => 'select',
      '#title' => $this->t('Ecwid Category'),
      '#options' => [null => 'Storefront (no category)', ...$options],
      '#default_value' => $this->configuration['ecwid_category'] ?? '',
      '#required' => false,
    ];

    return $form;
  }

  public function __construct(
    array $configuration,
    string $plugin_id,
    array $plugin_definition,
    protected readonly EcwidApiService $ecwidApiService,
    protected readonly ConfigFactoryInterface $configFactory,
    protected readonly LoggerChannelFactoryInterface $loggerChannelFactory,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition,
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('ecwid.ecwid_api_service'),
      $container->get('config.factory'),
      $container->get('logger.factory'),
    );
  }
}
