<?php declare(strict_types=1);

namespace Drupal\woolwich_ecwid\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a Ecwid Search block.
 *
 * @Block(
 *   id = "ecwid_search",
 *   admin_label = @Translation("Ecwid Search Block"),
 *   category = @Translation("Ecwid integration")
 * )
 */
class SearchBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $store_id = \Drupal::config('ecwid.settings')->get('store_id');

    return [
      '#theme' => 'ecwid_search_block',
      '#store_id' => $store_id,
    ];
  }

}
