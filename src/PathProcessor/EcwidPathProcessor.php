<?php declare(strict_types=1);

namespace Drupal\woolwich_ecwid\PathProcessor;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Link;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\PathProcessor\InboundPathProcessorInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\woolwich_ecwid\EcwidApiService;
use Drupal\woolwich_ecwid\Entity\EcwidPage;
use Drupal\path_alias\AliasManagerInterface;
use GuzzleHttp\Exception\ClientException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class EcwidPathProcessor implements
  InboundPathProcessorInterface,
  EventSubscriberInterface {

    use StringTranslationTrait;

  /**
   * The Ecwid store lives under a prefix in the URI.
   *
   * @throws \TypeError
   * If the module's state is incomplete or broken, the base path will be
   * empty or the wrong type. If this happens, the module must be
   * re-installed.
   */
  protected function getBasePath(): string {

    // Get the start of every store path, e.g. '/products'.
    $basePath = $this->configFactory
      ->get("ecwid.settings")
      ->get("store_base_path");

    // The store base path is required, so something has gone wrong if
    // it's empty.
    assert(
      !empty($basePath),
      new \TypeError("Ecwid store base path is not set. It is likely the Ecwid module is not installed properly, and should be re-installed")
    );

    return $basePath;
  }

  public function __construct(
    protected readonly ConfigFactoryInterface $configFactory,
    protected readonly EntityTypeManagerInterface $entityTypeManager,
    protected readonly AliasManagerInterface $aliasManager,
    protected readonly EcwidApiService $ecwidApiService,
    protected readonly LoggerChannelFactoryInterface $loggerFactory,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    // Register our event right before the redirect module redirects.
    // So our redirect is fired first.
    $events[KernelEvents::REQUEST][] = ["onKernelRequestNormaliseUrls", 31];

    return $events;
  }

  /**
   * Redirect to the path normalised by the path processor.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   The event to process.
   * 
   * @see \Drupal\woolwich_ecwid\PathProcessor\EcwidPathProcessor::processInbound()
   * 
   * @todo: Redirect requests bound for internal paths, like /ecwid-store/*
   */
  public function onKernelRequestNormaliseUrls(RequestEvent $event) {
    $request = $event->getRequest();
    $normalisedMaybe = $request->attributes->get(
      "_ecwid_route_normalised",
      FALSE
    );

    assert(
      is_string($normalisedMaybe) || is_bool($normalisedMaybe),
      new \TypeError(
        "'_ecwid_route_normalised' should be a string or boolean, Ecwid URL redirect in an unknown state"
      )
    );

    // ------------------------------------
    // From the redirect module (redirect/src/EventSubscriber/RouteNormalizerRequestSubscriber.php):
    //
    // The normalization can be disabled by setting the "_disable_route_normalizer"
    // request parameter to TRUE. However, this should be done before
    // onKernelRequestRedirect() method is executed.
    // ------------------------------------
    try {
      // `getBasePath()` can throw if config is incomplete, which happens
      // alarmingly often.
      if (strpos($request->getPathInfo(), $this->getBasePath()) === 0) {
        $request->attributes->set("_disable_route_normalizer", TRUE);
      }
    } catch (\TypeError $e) {
      $link = Link::createFromRoute(
        $this->t("Ecwid settings"),
        "ecwid.settings_form"
      )->toString();

      $this
        ->loggerFactory
        ->get("ecwid")
        ->critical(
          $this->t('Ecwid is missing some configuration. Check the @link.'),
          [
            "@link" => $link,
          ]
        );
    }

    if ($normalisedMaybe) {
      $queryString = $request->server->get("QUERY_STRING");
      $redirectUri =
        $request->getSchemeAndHttpHost() .
        $normalisedMaybe .
        (empty($queryString) ? "" : "?$queryString");

      $event->setResponse(new TrustedRedirectResponse(
        $redirectUri,
        301,
        ['X-Drupal-Ecwid-Route-Normaliser', '1'],
      ));

    }
  }

  /**
   * {@inheritdoc}
   */
  public function processInbound($path, Request $request): string {
    // See: https://drupal.stackexchange.com/questions/287070/why-does-this-route-work-with-a-slash-and-not-a-dash
    // for why all this is necessary. Drupal does not allow the flexible
    // routing Symfony provides.

    // If there is an alias for the given path, it is returned untouched.
    $pathAlias = $this->aliasManager->getPathByAlias($path);
    if ($pathAlias !== $path) {
      return $path;
    }

    // Do not attempt to process admin paths. Should also means users setting
    // their store base path to '/admin' has no effect.
    if ($path === "/admin" || strpos($path, "/admin/") === 0) {
      return $path;
    }

    try {
      $basePath = $this->getBasePath();
    } catch (\TypeError $e) {
      // If there's a problem with Ecwid's config, this will throw for every
      // URI in the content. So, the error is only logged in the kernel
      // request handler.
      return $path;
    }

    // Check the current path is part of the store.
    if ($path === $basePath || strpos($path, "$basePath/") !== 0) {
      return $path;
    }

    // Extract the remaining path, after the store base.
    $storePath = substr($path, strlen($basePath));

    $segments = $this->aliasSegments($this->normaliseCategories($this->segments($storePath)));

    // Reconstruct a normalised Ecwid URL from the `$segments` data structure.
    $normalisedUri = implode("/", [
      // Add the basePath back into the path, taking into account that a
      // URL alias will already include it.
      ...empty($segments['aliased'][0]['entity']['pathAlias']) ? [$basePath] : [],
      ...array_map(
        fn(array $category): string => $category['entity']['pathAlias'] ?? $category["slug"] .
        "-c" .
        $category["id"],
        $segments["aliased"]
      ),
      ...!empty($segments["defaultProduct"])
      ? [
        $segments["defaultProduct"]["slug"] .
        "-p" .
        $segments["defaultProduct"]["id"],
      ]
      : [],
    ]);

    // Sets up the event handler, so it can redirect the current path to
    // a normalised one. Ensures there's no duplicate content and keeps the
    // URLs on the site consistent.
    if ($path !== $normalisedUri) {
      $request->attributes->set("_ecwid_route_normalised", $normalisedUri);
    }
    else {
      $request->attributes->set("_ecwid_route_normalised", FALSE);
    }

    if (
      !empty($segments["defaultCategory"]) &&
      !empty($segments["defaultProduct"])
    ) {
      $path =
        "/ecwid-store/category/" .
        $segments["defaultCategory"]["id"] .
        "/product/" .
        $segments["defaultProduct"]["id"];

      // @see: getSubscribedEvents()
      $request->attributes->set("_disable_route_normalizer", TRUE);
    }
    elseif (!empty($segments["defaultProduct"])) {
      $path = "/ecwid-store/product/" . $segments["defaultProduct"]["id"];

      // @see: getSubscribedEvents()
      $request->attributes->set("_disable_route_normalizer", TRUE);
    }
    else if (!empty($segments["defaultCategory"])) {
      $path = "/ecwid-store/category/" . $segments["defaultCategory"]["id"];

      // @see: getSubscribedEvents()
      $request->attributes->set("_disable_route_normalizer", TRUE);
    }

    return $path;
  }

  /**
   * @todo: move to UrlParser class
   * @todo: use immutable value classes instead of assoc arrays
   */
  protected function aliasSegments(array $segments): array {
    $eq = $this->entityTypeManager
      ->getStorage("ecwid_page")
      ->getQuery()
      ->condition("status", 1)
      ->accessCheck();

    return [
      ...$segments,
      "aliased" => array_reduce(
        array_reverse(
          array_map(
            (fn(array $ecwidPages) => fn(array $segment) => [
              ...$segment,
              "entity" => match (
              (reset($ecwidPages) ?: NULL)?->get("ecwid_category")->value ===
              (string) $segment["id"]
            ) {
                TRUE => (fn(EcwidPage $ecwidPage) => [
                  "path" => $this->aliasManager->getPathByAlias($ecwidPage->toUrl()->toString()),
                  "pathAlias" => $ecwidPage->toUrl()->toString(),
                ])(reset($ecwidPages)),
                FALSE => ["path" => NULL, "pathAlias" => NULL],
              },
            ])(
              !empty($segments['normalised'])
              ?  $this->entityTypeManager
                ->getStorage("ecwid_page")
                ->loadMultiple(
                  $eq
                    ->condition(
                      "ecwid_category",
                      array_map(
                        fn(array $cat): int => $cat["id"],
                        $segments["normalised"]
                      ),
                      "IN"
                    )
                    ->execute()
                )
              : []
            ),
            $segments["normalised"]
          )
        ),
        fn(array $result, array $segment) => match (
        !empty ($segment["entity"]["path"]) &&
        $segment["entity"]["path"] !== $segment["entity"]["pathAlias"]
      ) {
          TRUE => [
            "aliased" => [$segment, ...$result["aliased"]],
            "aliasFound" => TRUE,
          ],
          FALSE => $result["aliasFound"]
          ? $result
          : [
            "aliased" => [$segment, ...$result["aliased"]],
            "aliasFound" => FALSE,
          ],
        },
        ["aliased" => [], "aliasFound" => FALSE]
      )["aliased"],
    ];
  }

  /**
   * @todo: move to UrlParser class
   * @todo: use immutable value classes instead of assoc arrays
   */
  private function getParents(array $category, array $categories = []) {
    return match (empty ($category["fromApi"]["parentId"])) {
      TRUE => array_map(
        fn(array $cat) => empty ($cat["id"])
        ? [
          "id" => $cat["fromApi"]["id"],
          "slug" => rawurlencode(
            str_replace(" ", "-", $cat["fromApi"]["name"])
          ),
          "fromApi" => $cat["fromApi"],
        ]
        : $cat,
        [$category, ...$categories]
      ),
      FALSE => $this->getParents(
        [
          "fromApi" => $this->ecwidApiService->getCategory(
            $category["fromApi"]["parentId"]
          ),
        ],
        [$category, ...$categories]
      ),
    };
  }

  /**
   * @todo: move to UrlParser class
   * @todo: use immutable value classes instead of assoc arrays
   */
  private function normaliseCategories(array $segments): array {
    return [
      ...$segments,
      "normalised" => array_reduce(
        array_map(
          fn(array $category) => [
            "slug" => $category["slug"],
            "id" => $category["id"],
            "fromApi" => $this->catOrNull($category["id"]),
          ],
          [...$segments["parentCategories"], ...array_filter([$segments["defaultCategory"]])]
        ),

        fn(array $categories, array $category) => empty($category["fromApi"])
        ? $categories
        : match (TRUE) {
          // No previous category,
          empty ($categories[array_key_last($categories)]) ||
          // or this category has no parent,
          empty ($category["fromApi"]["parentId"]) ||
          // or previous category's id is not equal to this category's
          // parent id.
          $categories[array_key_last($categories)]["id"] !==
          $category["fromApi"]["parentId"]
          => $this->getParents(
            $category
          ), // Rebuild the chain of categories.

          // The chain of categories is valid.
          $categories[array_key_last($categories)]["id"] ===
          $category["fromApi"]["parentId"]
          => [...$categories, $category],
        },

        []
      ),
    ];
  }

  /**
   * Helper function for producing arrays of filterable categories.
   *
   * If the category has been remove from Ecwid, it will resolve into `null`
   * and be removed from the path.
   */
  private function catOrNull(int $id): array|null {
    try {
      return $this->ecwidApiService->getCategory($id);
    }
    catch (ClientException $e) {
      return NULL;
    }
  }

  /**
   * @todo: move to UrlParser class
   * @todo: use immutable value classes instead of assoc arrays
   */
  protected function segments(string $path): array {
    return array_reduce(
      array_reverse(explode("/", trim($path, "/"))),
      fn(array $result, string $segment) => match (
      preg_match("/(.+)-(c|p)(\d+)/", $segment, $matches)
      ) {
        // Segment is a category or product.
        1 => match (TRUE) {
            // Segment is a category, and a category has not been found
            // previously.
            $matches[2] === "c" && empty ($result["defaultCategory"]) => [
              ...$result,
              "defaultCategory" => [
                "slug" => $matches[1],
                "id" => (int) $matches[3],
              ],
            ],
            // Segment is a category, and a category has already been found
            // previously.
            $matches[2] === "c" && !empty ($result["defaultCategory"]) => [
              ...$result,
              "parentCategories" => [
                [
                  "slug" => $matches[1],
                  "id" => (int) $matches[3],
                ],
                ...$result["parentCategories"],
              ],
            ],
            // Segment is a product. Unlike categories, there is no
            // hierarchy, so only the first product segment found is kept.
            $matches[2] === "p" && empty ($result["defaultProduct"]) => [
              ...$result,
              "defaultProduct" => [
                "slug" => $matches[1],
                "id" => (int) $matches[3],
              ],
            ],
            default => $result,
          },
        // Segment is not a category or a product.
        default => [
          ...$result,
          "aliasMaybe" => [$segment, ...$result["aliasMaybe"]],
        ],
      },
      [
        "parentCategories" => [],
        "defaultCategory" => [],
        "defaultProduct" => [],
        "aliasMaybe" => [],
      ]
    );
  }

}
